// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Snake->ScoreNum(Interactor,1);
			TransformFood(Interactor);
			
		}

	}
}

void AFood::TransformFood(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	float XFood, YFood;
	FVector MovementVector;
	CheckLocation = 0;
	while (CheckLocation  == 0)
	{
		XFood = rand() % 757 - 378;
		YFood = rand() % 757 - 378;
		FVector FoodVector = { XFood ,YFood , 0 };
		for (int i = 0; i < Snake->SnakeElements.Num(); i++)
		{
			auto ElementSnake = Snake->SnakeElements[i];
			FVector PrevLocation = ElementSnake->GetActorLocation();
			if (FoodVector == PrevLocation)
			{
				CheckLocation = 0;
			}
			else
			{
				CheckLocation = 1;
			}
		}
		MovementVector = FoodVector;
	}
	SetActorLocation(MovementVector);
	UE_LOG(LogTemp, Display, TEXT("Move Food!"));
}
