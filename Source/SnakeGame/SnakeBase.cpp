// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "BonusSpeed.h"
#include "LowSpeed.h"
#include "BigFood.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 5.f;
	
	LastMoveDirection = EMovementDirection::DOWN;
	LastTickMove = 0.f;
	Score = 0;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(4);
	SetActorTickInterval(MovementSpeed);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move(); 

}
void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i) 
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);

		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
	
}

void ASnakeBase::Move()
{
	FVector MovementVector;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		if (LastTickMove != 2)
		{
			LastTickMove = 1.f;
			MovementVector.X += ElementSize;
			CheckDirection = EMovementDirection::UP;
		}
		else
		{
			LastTickMove = 0.f;
		}
		break;
	case EMovementDirection::DOWN:
		if (LastTickMove != 1)
		{
			LastTickMove = 2.f;
			MovementVector.X -= ElementSize;
			CheckDirection = EMovementDirection::DOWN;
		}
		else
		{
			LastTickMove = 0.f;
		}
		break;
	case EMovementDirection::LEFT:
		if (LastTickMove != 4)
		{
			LastTickMove = 3.f;
			MovementVector.Y += ElementSize;
			CheckDirection = EMovementDirection::LEFT;
		}
		else
		{
			LastTickMove = 0.f;
		}
		break;
	case EMovementDirection::RIGHT:
		if (LastTickMove != 3)
		{
			LastTickMove = 4.f;
			MovementVector.Y -= ElementSize;
			CheckDirection = EMovementDirection::RIGHT;
		}
		else
		{
			LastTickMove = 0.f;
		}
		break;
	}
	if (LastTickMove == 0.f)
	{
		switch (CheckDirection)
		{
		case EMovementDirection::UP:
			LastTickMove = 1.f;
			MovementVector.X += ElementSize;
			LastMoveDirection = EMovementDirection::UP;
			break;
		case EMovementDirection::DOWN:
			LastTickMove = 2.f;
			MovementVector.X -= ElementSize;
			LastMoveDirection = EMovementDirection::DOWN;
			break;
		case EMovementDirection::LEFT:
			LastTickMove = 3.f;
			MovementVector.Y += ElementSize;
			LastMoveDirection = EMovementDirection::LEFT;
			break;
		case EMovementDirection::RIGHT:
			LastTickMove = 4.f;
			MovementVector.Y -= ElementSize;
			LastMoveDirection = EMovementDirection::RIGHT;
			break;
		}


	}


	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement -> GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	
}

void ASnakeBase::MaxSpeed()
{
	FTimerHandle Handel;
	SetActorTickInterval(MovementSpeed/2);
	GetWorld()->GetTimerManager().SetTimer(Handel, this, &ASnakeBase::NormalSpeed, 5.f, false);
	UE_LOG(LogTemp,Display,TEXT("Speed!"));
}

void ASnakeBase::MinSpeed()
{
	FTimerHandle Handel;
	SetActorTickInterval(MovementSpeed*2);
	GetWorld()->GetTimerManager().SetTimer(Handel, this, &ASnakeBase::NormalSpeed, 5.f, false);
	UE_LOG(LogTemp, Display, TEXT("Low!"));
}

void ASnakeBase::NormalSpeed()
{
	SetActorTickInterval(MovementSpeed);
	UE_LOG(LogTemp, Display, TEXT("Normal!"));
}

void ASnakeBase::CreateBonusSpeed(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	float XFood, YFood;
	FVector MovementVector;
	CheckLocation = 0;
	while (CheckLocation == 0)
	{
		XFood = rand() % 757 - 378;
		YFood = rand() % 757 - 378;
		FVector FoodVector = { XFood ,YFood , 0 };
		for (int i = 0; i < Snake->SnakeElements.Num(); i++)
		{
			auto ElementSnake = Snake->SnakeElements[i];
			FVector PrevLocation = ElementSnake->GetActorLocation();
			if (FoodVector == PrevLocation)
			{
				CheckLocation = 0;
			}
			else
			{
				CheckLocation = 1;
			}
		}
		MovementVector = FoodVector;
	}
	FTransform NewTransform(MovementVector);
	ABonusSpeed* NewBonusSpeed = GetWorld()->SpawnActor<ABonusSpeed>(BonusSpeedClass, NewTransform);
	UE_LOG(LogTemp, Display, TEXT("Move Food!"));
}

void ASnakeBase::CreateLowSpeed(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	float XFood, YFood;
	FVector MovementVector;
	CheckLocation = 0;
	while (CheckLocation == 0)
	{
		XFood = rand() % 757 - 378;
		YFood = rand() % 757 - 378;
		FVector FoodVector = { XFood ,YFood , 0 };
		for (int i = 0; i < Snake->SnakeElements.Num(); i++)
		{
			auto ElementSnake = Snake->SnakeElements[i];
			FVector PrevLocation = ElementSnake->GetActorLocation();
			if (FoodVector == PrevLocation)
			{
				CheckLocation = 0;
			}
			else
			{
				CheckLocation = 1;
			}
		}
		MovementVector = FoodVector;
	}
	FTransform NewTransform(MovementVector);
	ALowSpeed* NewLowSpeed = GetWorld()->SpawnActor<ALowSpeed>(LowSpeedClass, NewTransform);
	UE_LOG(LogTemp, Display, TEXT("Move Food!"));
}

void ASnakeBase::ScoreNum(AActor* Interactor, int scr)
{
	numScore++;
	Score += scr;
	int nBonus,nRand;
	nBonus = rand() % 3;
	switch (nBonus)
	{
	case 0:

		break;
	case 1:
		nRand = rand() % 100;
		if (nRand < 20) 
		{
			CreateBonusSpeed(Interactor);
		}
		break;
	case 2:
		nRand = rand() % 100;
		if (nRand < 20)
		{
			CreateLowSpeed(Interactor);
		}
		break;
	default:
		break;
	}

	if (numScore % 5 == 0)
	{
		CreateBigFood(Interactor);
	}
	UE_LOG(LogTemp, Display, TEXT("+1"));
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement,AActor* Other)
{
	if (IsValid(OverlappedElement)) 
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);

		}
	}
}

void ASnakeBase::CreateBigFood(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	float XFood, YFood;
	FVector MovementVector;
	CheckLocation = 0;
	while (CheckLocation == 0)
	{
		XFood = rand() % 757 - 378;
		YFood = rand() % 757 - 378;
		FVector FoodVector = { XFood ,YFood , 0 };
		for (int i = 0; i < Snake->SnakeElements.Num(); i++)
		{
			auto ElementSnake = Snake->SnakeElements[i];
			FVector PrevLocation = ElementSnake->GetActorLocation();
			if (FoodVector == PrevLocation)
			{
				CheckLocation = 0;
			}
			else
			{
				CheckLocation = 1;
			}
		}
		MovementVector = FoodVector;
	}
	FTransform NewTransform(MovementVector);
	ABigFood* NewBigFood = GetWorld()->SpawnActor<ABigFood>(BigFoodClass, NewTransform);
	UE_LOG(LogTemp, Display, TEXT("Move Food!"));
}


