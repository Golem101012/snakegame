// Fill out your copyright notice in the Description page of Project Settings.


#include "LowSpeed.h"
#include "SnakeBase.h"

// Sets default values
ALowSpeed::ALowSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALowSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALowSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALowSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->MinSpeed();
			Snake->ScoreNum(Interactor,5);
			Destroy();
		}

	}
}

