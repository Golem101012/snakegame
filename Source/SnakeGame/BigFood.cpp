// Fill out your copyright notice in the Description page of Project Settings.


#include "BigFood.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ABigFood::ABigFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ABigFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABigFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABigFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Snake->ScoreNum(Interactor, 10);
			Destroy();

		}

	}
}





