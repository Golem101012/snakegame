// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"


class ABigFood;
class ABonusSpeed;
class ALowSpeed;
class ASnakeElementBase;




UENUM()
enum class  EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};




UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		UClass* BigFoodClass = Cast<UBlueprint>(StaticLoadObject(UBlueprint::StaticClass(), nullptr, TEXT("/Script/Engine.Blueprint'/Game/BluePrints/BigFoodBP.BigFoodBP'")))->GeneratedClass;
		//TSubclassOf<ABigFood> BigFoodClass;

	UPROPERTY(EditDefaultsOnly)
		UClass* BonusSpeedClass = Cast<UBlueprint>(StaticLoadObject(UBlueprint::StaticClass(), nullptr, TEXT("/Script/Engine.Blueprint'/Game/BluePrints/BonusSpeedBP.BonusSpeedBP'")))->GeneratedClass;

	UPROPERTY(EditDefaultsOnly)
		UClass* LowSpeedClass = Cast<UBlueprint>(StaticLoadObject(UBlueprint::StaticClass(), nullptr, TEXT("/Script/Engine.Blueprint'/Game/BluePrints/LowSpeedBP.LowSpeedBP'")))->GeneratedClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;


	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;
	UPROPERTY()
		EMovementDirection LastMoveDirection;
	UPROPERTY()
		EMovementDirection CheckDirection;
	UPROPERTY()
	float LastTickMove;
	UPROPERTY()
		int32 Score;
	UPROPERTY()
		int numScore;

	UPROPERTY()
		int CheckLocation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);

	void Move();
	
	void MaxSpeed();

	void MinSpeed();

	void NormalSpeed();

	void CreateBonusSpeed(AActor* Interactor);

	void CreateLowSpeed(AActor* Interactor);

	void ScoreNum(AActor* Interactor, int scr);

	void CreateBigFood(AActor* Interactor);

	void DestroyBigFood(ABigFood* NewBigFood);

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
};
